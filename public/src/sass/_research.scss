/*------------ RESEARCH ------------*/

.overview1-section {
	padding: 60px 0 50px 0;

	@media screen and (max-width : 767px) {
		padding-bottom: 0px;
	}

	.intro {
		margin-bottom: 40px;

		h2 {
			text-align: center;
		}

		.two-column-p {
			column-count: 2;
			column-gap: 30px;

			@media screen and (max-width : 767px) {
				column-count: auto;
				column-gap: auto;
			}
		}

		p {
			margin-bottom: 10px;
		}
	}

	.campus {
		margin-bottom: 16px;

		@media screen and (max-width : 767px) {
			margin-bottom: 0px;
		}

		img {
			width: 100%;
			margin-bottom: 14px;
			@extend .standard_transition;

			&:hover {
				opacity: .8;
			}
		}

		h3 {
			@extend .subheading-copy;

			.fa {
				margin-left: 10px;
			}
		}

		.col-md-10 >h3 {
			text-align: center;
		}

		.options {
			padding-left: 0px;
			padding-right: 0px;
			overflow: auto;
		}

		.red-btn {
			margin: 0 auto;

			@media screen and (max-width : 767px) {
				width: 100%;
				text-align: center;
				line-height: 20px;
				padding: 10px 15px;
			}
		}
	}
}

.lrg-filter-section {
	background-image: url(images/research-currentresearch-background.jpg);
	background-size: cover;
	background-repeat: no-repeat;
	padding: 70px 0;

	.intro {
		text-align: center;
		margin-bottom: 44px;
	}

	.campus-inline-menu {
		margin-bottom: 40px;

		li {
			margin-right: 14px;

			a {
				color: $white;

				&:hover, &:focus, &.toggle {
					color: $nebraskared;
					background-color: $white;
					text-decoration: none;
				}
			}
		}
	}
}

.filter-list {

	.node {
		@media screen and (max-width : 767px) {
			padding: 0 30px;
		}
	}
}

.research-filter-module {

	.overlay {
		height: calc(100% - 70px);
	}

	img {
		display: block;
	}

	.bottom {
		height: 70px;
		padding: 0px 15px;
	}
}

.slider-3-col-wrap {
	padding: 100px 0;

	background-image: url(images/research-facilities-lattice1.svg),url(images/research-facilities-lattice2.svg);
    background-repeat: no-repeat, no-repeat;
    background-position: top 0px left 0px, bottom 0px right 0px;
    background-size: 292px, 381px;

    @media screen and (max-width : 991px) {
    	background-image: none;
    	padding: 70px 0 100px 0; 
    }

	@media screen and (max-width : 767px) {
		padding: 32px 0;
	}

	.intro {
		text-align: center;
		margin-bottom: 60px;

		@media screen and (max-width : 767px) {
			margin-bottom: 30px;
		}
	}

	.slider-3-col {
		padding: 0px;

		.navigator {
			background: #e0e0e0;
			@extend .standard_transition;

			&:hover {
				background-color:rgba(208,13,44,0.5);
			}
		}
	}

	.thumb {
		@extend .img-treatment1;
		margin-bottom: 20px;

		@media screen and (max-width : 767px) {
			margin-bottom: 50px;
		}
	}

	h3 {
		@extend .subheading-copy;
		margin-bottom: 10px;

		@media screen and (max-width : 767px) {
			text-align: center;
		}
	}

	p {
		@include futura-pt-format(16px, 400, 20px, $black, 0px);
	}

	.slick-dots {
		bottom: -60px;

		@media screen and (max-width : 767px) {
			top: 300px;
			bottom: auto;
		}

		@media screen and (max-width : 370px) {
			top: 290px;
		}

		@media screen and (max-width : 350px) {
			top: 250px;
		}
	}
}

.slider-4-col-wrap {
	background-image: url(images/research-awards-background.jpg);
	background-size: cover;
    background-repeat: no-repeat;
    padding: 60px 0 120px 0;

    .intro {
    	text-align: center;
    	margin-bottom: 40px;
    }
}

.awardee-module {

	.bottom {
		height: auto;
		padding: 10px 15px;

		h3 {

			span {
				font-size: 16px;
				line-height: normal;
				font-style: normal;
			}
		}
	}

	&:hover {

		img {
			transform: none;
		}
	}
}

.slider-4-col {
	padding-left: 0px;
	padding-right: 0px;

	.slick-dots {

		li {

			button {
				background-color: $white;
				border-color: $white;
			}

			&.slick-active button {
				background-color: $nebraskared;
				border-color: $nebraskared;
			}
		}
	}
}