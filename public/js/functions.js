var $sitewrap  = $('#wrapper'),
	$mobilebtn = $('#nav-toggle'),
	$mobilenav = $('#mobile-nav'),
	$searchform= $('#header #searchform'),
	$searchbtn = $('#header .bottom .search-btn'),
	$searchclsr= $('#header #searchform .closer'),
	$srchfltrbtn=$('.search-inline-menu li'),
	$sixgoaltop= $('.full-accordian-template.list .drawer-node .top');

$(window).scroll( function() {
	var value = $(this).scrollTop(),
		topcut = $('.article-template .row:first-child').outerHeight() + 20 - $('.mediacontact-module.main').outerHeight();
	if ( value > $('#header').outerHeight() + $('#hero').outerHeight() ) {
		$(".mediacontact-module.main").addClass("moonwalk");
	} else {
		$(".mediacontact-module.main").removeClass("moonwalk");
	}

	// Does the math on how far is the top of the page from the top of the "modules" div on the article page which holds related articles and tags. Subtracts the height of your fixed object.
	// When you scroll to this pixel point from that math, your fixed object stops in place.
	if ( value + $('.mediacontact-module.main').outerHeight() + 70 > $('.modules').offset().top ) {
		$(".mediacontact-module.main").css("position","absolute");
		$(".mediacontact-module.main").css("top", topcut);
	} else {
		$(".mediacontact-module.main").css("position","fixed");
		$(".mediacontact-module.main").css("top", "70px");
	}

	//Returns the node to 'static' or regular positioning when the user scrolls to the top of it's containing div
	if (value < $('#header').outerHeight() + $('#hero').outerHeight() ) {
		$(".mediacontact-module.main").css("position","static");
	}
});

$mobilebtn.on('mousedown', function() {
	var $this = $(this);

	$this.toggleClass('active');

	if ($this.hasClass('active')) {
		$mobilenav.addClass('toggle');
		$sitewrap.addClass('toggle');
	} else {
		$mobilenav.removeClass('toggle');
		$sitewrap.removeClass('toggle');
	}
});

$searchclsr.on('click', function() {
	$searchform.removeClass('active').velocity("fadeOut");
});

$searchbtn.on('click', function() {
	if ($searchform.hasClass('active')) {
		$searchform.removeClass('active').velocity("fadeOut");
	} else {
		$searchform.addClass('active').velocity("fadeIn");
	}
});

$sixgoaltop.on('click', function( event ) {
	event.preventDefault();
	
	var $this = $(this);

	$this.parent().siblings().removeClass('toggle');
	$this.parent().toggleClass('toggle');
});

$srchfltrbtn.on('click', function() {
	var $this = $(this);

	$this.children('a').addClass('toggle');
	$this.siblings().children('a').removeClass('toggle');
});

$($sixgoaltop,'.search-inline-menu li a').on('click', function( event ) {
	event.preventDefault();
});

$(function() {
	// quick search regex
	var qsRegex;
	var $filterbtn = $('.filter li');

	var $container = $('.filter-list, .roster-list, .stat-panels').isotope({
		itemSelector: '.node, .stat',
		transitionDuration: '0.8s',
		percentPosition: true,
		filter: function() {
		  return qsRegex ? $(this).text().match( qsRegex ) : true;
		}
	});

	// use value of search field to filter
	$('#quicksearch').on('focus', function() {
	$container.isotope({
		itemSelector: '.filter-node',
		layoutMode: 'masonry',
		filter: function() {
		  return qsRegex ? $(this).text().match( qsRegex ) : true;
		}
	});
	var $quicksearch = $('#quicksearch').keyup( debounce( function() {
		qsRegex = new RegExp( $quicksearch.val(), 'gi' );
		$container.isotope();
		}, 200 ) );
	});
	
	$container.imagesLoaded( function() {
	 	$container.isotope('layout');
	});

	$filterbtn.on('click', function() {
		var $this = $(this);
		var $filtering = '.' + $this.attr('class');

		$container.isotope({ filter: $filtering });
		$this.children('a').addClass('toggle');
		$this.siblings().children('a').removeClass('toggle');
	});
});

$(document).ready(function() {
	svg4everybody();
	
	$('#affordability .filter .kearney').click();
	$('#alumni-students .filter .alumni').click();

	$('#hero.slider').slick({
		autoplay: true,
		dots: true,
		autoplaySpeed: 9000,
		prevArrow: $('.slider-con.hero .navigator.prev'),
		nextArrow: $('.slider-con.hero .navigator.next')
	});

	$('.slider.history').slick({
		autoplay: true,
		dots: true,
		autoplaySpeed: 9000,
		prevArrow: $('.slider-con.history .navigator.prev'),
		nextArrow: $('.slider-con.history .navigator.next')
	});

	$('.slider.transformation').slick({
		autoplay: true,
		dots: true,
		autoplaySpeed: 9000,
		prevArrow: $('.slider-con.transformation .navigator.prev'),
		nextArrow: $('.slider-con.transformation .navigator.next')
	});

	$('.slider.communityimpact').slick({
		autoplay: true,
		dots: true,
		autoplaySpeed: 9000,
		prevArrow: $('.slider-con.communityimpact .navigator.prev'),
		nextArrow: $('.slider-con.communityimpact .navigator.next')
	});

	$('.slider.slider-3-col').slick({
		autoplay: true,
		dots: true,
		autoplaySpeed: 9000,
		slidesToShow: 3,
		slide:'.slide',
		prevArrow: $('.slider-con.slider-3-col .navigator.prev'),
		nextArrow: $('.slider-con.slider-3-col .navigator.next'),
		responsive: [
		{
		  breakpoint: 767,
		  settings: {
		  	slidesToShow: 1,
		  	swipe: true
		  }
		}
		]
	});

	$('.slider.slider-4-col').slick({
		autoplay: true,
		dots: true,
		autoplaySpeed: 9000,
		slidesToShow: 4,
		slide:'.slide',
		prevArrow: $('.slider-con.slider-4-col .navigator.prev'),
		nextArrow: $('.slider-con.slider-4-col .navigator.next'),
		responsive: [
		{
		  breakpoint: 991,
		  settings: {
			  slidesToShow: 3,
			  swipe: true,
			  arrows: true,
			  prevArrow: $('.slider-con.slider-4-col .navigator.prev'),
			  nextArrow: $('.slider-con.slider-4-col .navigator.next')
		  }
		},
		{
		  breakpoint: 767,
		  settings: {
		  	slidesToShow: 1,
		  	swipe: true
		  }
		}
		]
	});

	$('.slider.slide-filter-list').slick({
		autoplay: true,
		dots: true,
		autoplaySpeed: 9000,
		slidesToShow: 3,
		slide:'.node',
		prevArrow: $('.slider-con.slide-filter-list .navigator.prev'),
		nextArrow: $('.slider-con.slide-filter-list .navigator.next'),
		responsive: [
		{
		  breakpoint: 991,
		  settings: {
			  slidesToShow: 2,
			  swipe: true,
			  arrows: true
		  }
		},
		{
		  breakpoint: 767,
		  settings: {
		  	slidesToShow: 1,
		  	swipe: true
		  }
		}
		]
	});

	$('.slider-3-col-filter-wrap .campus-inline-menu li').on('click', function() {
		var $this = $(this);
		var $filtering = '.' + $this.attr('class');

		$('.slider.slide-filter-list').slick('slickFilter', $filtering);
		$this.children('a').addClass('toggle');
		$this.siblings().children('a').removeClass('toggle');
	});

	$('.slider.graduates').slick({
		mobileFirst: true,
		autoplay: true,
		dots: true,
		autoplaySpeed: 9000,
		slide:'col-sm-3',
		responsive: [
		{
		  breakpoint: 767,
		  settings: 'unslick'
		}
		]
	});
});
//# sourceMappingURL=maps/functions.js.map
