var gulp = require('gulp'),
    sass = require('gulp-sass'),
    sourcemaps = require('gulp-sourcemaps'),
    autoprefixer = require('gulp-autoprefixer'),
    concat = require('gulp-concat'),
    svgSprite = require('gulp-svg-sprite'),
    notify = require('gulp-notify'),
    insert = require('gulp-insert'),
    mygulplogo = __dirname + '/src/gulpimages/gulplogo' + Math.floor((Math.random() * 3) + 1) + '.jpg';

// SVG Sprites
gulp.task('svg-sprite', function() {
  return gulp.src('./src/svg/*.svg')
    .pipe(svgSprite({
      mode: {
        symbol: {
          dest: '',
          prefix: '',
          sprite: 'spritemap'
        }
      }
    }))
    .pipe(gulp.dest('./images'));
});

// CSS
gulp.task('styles', function () {
  return gulp.src('./src/sass/*.scss')
    .pipe(sourcemaps.init())
      .pipe(sass({outputStyle: 'compressed'}).on('error', sass.logError))
      .pipe(autoprefixer({
          browsers: ['last 2 versions', 'safari 6', 'ie 9', 'ios 7', 'android 4']
        }))
    .pipe(sourcemaps.write('./'))
    .pipe(insert.prepend('/*  \n Theme Name: Untitled Document \n Theme URI: http://something.com \n Description: Description here \n Version: 0.5 \n Author: Gregory Spraggins for 160over90 \n Author URI: http://160over90.com \n */ \n \n'))
    .pipe(gulp.dest('./'))
    .pipe(notify({
      message: "Sassy swag is a go. Your stylesheet has been awesomemized.",
      icon: mygulplogo
    }));
});

// JS - custom scripts
gulp.task('scripts', function() {
  return gulp.src('./src/js/functions.js')
    .pipe(sourcemaps.init())
      .pipe(concat('functions.js'))
    .pipe(sourcemaps.write('./maps'))
    .pipe(gulp.dest('./js'))
    .pipe(notify({
      message: "JS swag is a go. Your javascript has been awesomemized.",
      icon: mygulplogo
    }));
});

// JS - plugins
gulp.task('scripts-plugin', function() {
  return gulp.src('./src/js/plugins/*.js')
    .pipe(sourcemaps.init())
      .pipe(concat('plugins.js'))
    .pipe(sourcemaps.write('./maps'))
    .pipe(gulp.dest('./js'));
});

// Copy files to dist
gulp.task('copyTask', function() {
  // JS library
  gulp.src('./src/js/lib/**/*')
    .pipe(gulp.dest('./js/lib'));
});

// Default Tasks
gulp.task('default', ['copyTask', 'styles', 'scripts', 'svg-sprite','scripts-plugin']);

// Watch tasks
gulp.task('watch', function() {

  // Watch .scss files
  gulp.watch('src/sass/**/*.scss', ['styles']);

  // Watch scripts.js files
  gulp.watch('src/js/functions.js', ['scripts']);

  // Watch plugin .js files
  gulp.watch('src/js/plugins/*.js', ['scripts-plugin']);

  // Watch for .js library files
  gulp.watch('src/js/lib/*.js', ['copyTask']);

  // SVG files for spritemap
  gulp.watch('src/svg/*.svg', ['svg-sprite']);;
});
