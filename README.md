# UNEB-141-11w University of Nebraska Development

Website for University of Nebraska's main .edu

## Dependencies
+ Node.js

## Install using NPM

```bash
$ npm install
```

## Process javascript, sass and any gulp tasks

```bash
$ gulp
```

## Watch javascript, sass and any gulp tasks

```bash
$ gulp watch
```

## Add SVG to spritemap

Drop any svg into the `/src/svg` folder. The name of the svg will become the id of the svg. Edit SVG element via CSS using fill.

```bash
$ gulp watch
```

## JavaScript
### File structure
* `/src/plugins` -> `/js/plugins.js` -- All files get concat, and minified into one plugins.js
* `/src/functions.js` -> `/js/functions.js` -- Minified